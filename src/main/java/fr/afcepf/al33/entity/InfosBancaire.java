package fr.afcepf.al33.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "InfosBancaire")
@NamedQuery(name = "InfosBancaire.getByNumeroCarte", query = "SELECT i FROM InfosBancaire i " + "WHERE (i.numeroCarte like ?1)")
public class InfosBancaire implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idInfosBancaire;
	
	@ManyToOne
	@JoinColumn(name = "idClient", nullable = false)//, foreignKey = @ForeignKey(name = "FK_Client_InfosBancaires"))
	private Client client;
	
	@Column(nullable = false, length = 16)
	private String numeroCarte;

	@Column(nullable = false)
	private int moisExpiration;
	
	@Column(nullable = false)
	private int anneeExpiration;
	
	@Column(nullable = false)
	private String cryptogramme;
	
	@Column(nullable = false)
	private boolean estActive;
	
	@ManyToOne
	@JoinColumn(name = "idCompte", nullable = false, foreignKey = @ForeignKey(name = "FK_Compte_InfosBancaires"))
	private CompteBancaire compte;

	public InfosBancaire() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InfosBancaire(Integer idInfosBancaire, Client client, String numeroCarte, int moisExpiration,
			int anneeExpiration, String cryptogramme, boolean estActive, CompteBancaire compte) {
		super();
		this.idInfosBancaire = idInfosBancaire;
		this.client = client;
		this.numeroCarte = numeroCarte;
		this.moisExpiration = moisExpiration;
		this.anneeExpiration = anneeExpiration;
		this.cryptogramme = cryptogramme;
		this.estActive = estActive;
		this.compte = compte;
	}

	public Integer getIdInfosBancaire() {
		return idInfosBancaire;
	}

	public void setIdInfosBancaire(Integer idInfosBancaire) {
		this.idInfosBancaire = idInfosBancaire;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public String getNumeroCarte() {
		return numeroCarte;
	}

	public void setNumeroCarte(String numeroCarte) {
		this.numeroCarte = numeroCarte;
	}

	public int getMoisExpiration() {
		return moisExpiration;
	}

	public void setMoisExpiration(int moisExpiration) {
		this.moisExpiration = moisExpiration;
	}

	public int getAnneeExpiration() {
		return anneeExpiration;
	}

	public void setAnneeExpiration(int anneeExpiration) {
		this.anneeExpiration = anneeExpiration;
	}

	public String getCryptogramme() {
		return cryptogramme;
	}

	public void setCryptogramme(String cryptogramme) {
		this.cryptogramme = cryptogramme;
	}

	public boolean isEstActive() {
		return estActive;
	}

	public void setEstActive(boolean estActive) {
		this.estActive = estActive;
	}

	public CompteBancaire getCompte() {
		return compte;
	}

	public void setCompte(CompteBancaire compte) {
		this.compte = compte;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "InfosBancaires [idInfosBancaire=" + idInfosBancaire + ", client=" + client + ", numeroCarte="
				+ numeroCarte + ", moisExpiration=" + moisExpiration + ", anneeExpiration=" + anneeExpiration
				+ ", cryptogramme=" + cryptogramme + ", estActive=" + estActive + ", compte=" + compte + "]";
	}
	

	
	
	
}
