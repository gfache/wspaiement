package fr.afcepf.al33.entity;

public enum MessageEnum {

	authenticité("Informations non correctes"),
	carteInconnu("Numéro de carte inconnu"),
	carteInvalide("Carte non valide"),
	nonSolvable("Problème de solvabilité"),
	ok("Transaction acceptée"),
	problemeTransaction("Problème lors de la transaction, transaction annulée"),
	mauvaisIban("Compte non trouvé"),
	transactionOK("Transaction effectuée");
	
	private String name;
	
	private MessageEnum(String name) {
		this.name = name;
	}
	
	public String toString() {
		return name;
	}
}
