package fr.afcepf.al33.IDao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afcepf.al33.entity.CompteBancaire;

public interface IDaoCompteBancaire extends JpaRepository<CompteBancaire, Integer> {
	CompteBancaire findByIban(String iban);
}
