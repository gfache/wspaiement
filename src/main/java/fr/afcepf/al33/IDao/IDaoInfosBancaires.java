package fr.afcepf.al33.IDao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afcepf.al33.entity.InfosBancaire;

public interface IDaoInfosBancaires extends JpaRepository<InfosBancaire, Integer> {

	InfosBancaire getByNumeroCarte(String numeroCarte);
}
