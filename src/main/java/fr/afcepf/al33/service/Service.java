package fr.afcepf.al33.service;

import java.util.Calendar;


import fr.afcepf.al33.IDao.IDaoTransaction;
import fr.afcepf.al33.entity.Transaction;
import fr.afcepf.al33.util.MD5Transaction;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import fr.afcepf.al33.IDao.IDaoCompteBancaire;
import fr.afcepf.al33.IDao.IDaoInfosBancaires;
import fr.afcepf.al33.dto.AutorisationDTO;
import fr.afcepf.al33.dto.InfosBancaireDTO;
import fr.afcepf.al33.dto.InfosTransactionDTO;
import fr.afcepf.al33.entity.CompteBancaire;
import fr.afcepf.al33.entity.InfosBancaire;
import fr.afcepf.al33.entity.MessageEnum;
import fr.afcepf.al33.exception.CreditException;
import fr.afcepf.al33.exception.DebitException;
import fr.afcepf.al33.web.PaiementMb;

@org.springframework.stereotype.Service
@Transactional
public class Service implements IService {

	@Autowired
	private IDaoInfosBancaires daoInfosBancaire;
	
	@Autowired
	private IDaoCompteBancaire daoCompteBancaire;

	@Autowired
	private IDaoTransaction daoTransaction;
	
	public AutorisationDTO validationPaiement(InfosBancaireDTO infoPaiement) {
		InfosBancaire infosBancaire = daoInfosBancaire.getByNumeroCarte(infoPaiement.getNumeroCarte());
		AutorisationDTO autorisationDTO = new AutorisationDTO();		
	if(infosBancaire == null) {
		autorisationDTO.setMessage(MessageEnum.carteInconnu.toString());
		autorisationDTO.setEstAccepte(false);
		return autorisationDTO;
	}
	else {
		//verif validite carte
		if(verifValidite(infosBancaire) == false) {
			autorisationDTO.setMessage(MessageEnum.carteInvalide.toString());
			autorisationDTO.setEstAccepte(false);
			return autorisationDTO;
		}
		if(verifSolvabilite(infosBancaire, infoPaiement.getMontant())==false) {
			autorisationDTO.setMessage(MessageEnum.nonSolvable.toString());
			autorisationDTO.setEstAccepte(false);
			return autorisationDTO;
		}
		//si tout est ok :
		autorisationDTO.setMessage(MessageEnum.ok.toString());
		autorisationDTO.setEstAccepte(true);
		CompteBancaire compteADebiter = daoCompteBancaire.getOne(infosBancaire.getCompte().getIdCompte());
		try {
			debiter(infoPaiement.getMontant(), compteADebiter);
			
		} catch (DebitException e) {
			autorisationDTO.setMessage(MessageEnum.problemeTransaction.toString());
			autorisationDTO.setEstAccepte(false);
		}
		return autorisationDTO;
		}
	}
	
	private void debiter(double montant, CompteBancaire compteAcheteur) throws DebitException {
		double nouveauSolde = compteAcheteur.getSolde() - montant;
		compteAcheteur.setSolde(nouveauSolde);
		daoCompteBancaire.save(compteAcheteur);
	}
	
	
	private boolean verifValidite(InfosBancaire infosBancaire) {
		int annee = Calendar.getInstance().get(Calendar.YEAR);
		int mois = Calendar.getInstance().get(Calendar.MONTH)+1;
		System.out.println("mois : " + mois);
		System.out.println("annee : " + annee);
		if(infosBancaire.getAnneeExpiration() > annee || (infosBancaire.getAnneeExpiration() == annee && infosBancaire.getMoisExpiration() >= mois)) {
			return true;
		} else {
			return false;
		}
	}
	
	private boolean verifSolvabilite(InfosBancaire infosBancaire, double montant) {
		CompteBancaire compteAssocie = daoCompteBancaire.getOne(infosBancaire.getCompte().getIdCompte());
		if(montant <= compteAssocie.getSolde()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Transaction creerTransaction(Transaction transaction) {
		transaction = daoTransaction.save(transaction);
		String hash = MD5Transaction.getEncodedId(transaction.getIdTransaction().toString());
		transaction.setHash(hash);
		return daoTransaction.save(transaction);
	}

	@Override
	public void validerTransaction(Transaction transaction) {
		transaction.setPaye(true);
		daoTransaction.save(transaction);
	}
}
