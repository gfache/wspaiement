package fr.afcepf.al33.service;

import fr.afcepf.al33.dto.AutorisationDTO;
import fr.afcepf.al33.dto.InfosBancaireDTO;
import fr.afcepf.al33.entity.Transaction;

public interface IService {

	AutorisationDTO validationPaiement(InfosBancaireDTO infoBancaire);

    Transaction creerTransaction(Transaction transaction);

    void validerTransaction(Transaction transaction);
}
